# Structure

## TR_TR

Contains diagram(s) showing the interdependencies of different technical
specifications.

## TR_REG

Contains diagram(s) showing the interdependencies of different technical
specifications to german and european regulatory documents.

## eID_Architecture

This diagram gives an overview over the architecture of the german eID and it's
connection to the eIDAS network. Additionally the diagram lists the
corresponding german and european specifications of most of the entities and
precesses.

### Soucers

For the creation of this diagram the following documents have been used (this  
does not containt all standards mentioned in the diagram):

- TR_03110[1-4]
- TR_03120-[1,2]
- TR_03127
- TR_03130-[1-3]
- TR_03159-2

# Scope

## Technical Specification

### German

The specifications used in the diagrams TR_TR and TR_REG are the
following:

- Umbrella specifications:
  - TR-03127 - eID-Karten mit eID- und eSign-Anwendung basierend auf
    Extended Access Control
  - TR-03159 - Mobile Identities
- Chip specification: TR-03110 - Advanced Security Mechanisms for
  Machine Readable Travel Documents
- Signatures: TR-03117 - eCards mit kontaktloser Schnittstelle
  als qualifizierte Signaturerstellungseinheit
- eID-Infrastruktur:
  - Client: TR-03124 - eID-Client
  - Server: TR-03130 - Technical Guideline eID-Server
  - Framework:  TR-03112 - eCard-API-Framework
  - PKI:
    - TR-03129
    - Certificate Policies (CPs):
      - CP-CSCA (listed for completeness but not public)
      - CP-eID
      - CP-eSign
      - CP-ePass
- technically not functionality but containting level of assurance
  considerations:
  - TR-03107 - Elektronische Identitäten und Vertrauensdienste im
    E-Government
  - TR-03147 - Vertrauensniveaubewertung von Verfahren zur
    Identitätsprüfung natürlicher Personen

These specifications have been chosen since the author thinks that those
represent the core functionality.

The following specifications are listed as eID concerning by the
[BSI](https://www.bsi.bund.de/ElektronischeAusweiseTR) but do not
describe core functionality and are therefore out of scope:

- TR-02102 - Kryptographische Verfahren: Empfehlungen und
  Schlüssellängen
- TR-03104 - Produktionsdatenerfassung, -qualitätsprüfung und
  -übermittlung für Pässe
- TR-03105 - Conformity Tests for Official Electronic ID Documents
- TR-03111 - Elliptic Curve Cryptography
- TR-03116 - Kryptographische Vorgaben für Projekte der Bundesregierung
- TR-03118 - Prüfspezifikation Biometrie
- TR-03119 - Requirements for Smart Card Readers Supporting eID and
  eSign Based on Extended Access Control
- TR-03121 - Technical Guideline Biometrics for Public Sector
  Applications
- TR-03122 - Conformance Test Specification for Technical Guideline
  TR-03121 Biometrics for Public Sector Applications
- TR-03123 - XML-Datenaustauschformat für hoheitliche Dokumente (TR XhD)
- TR-03131 - EAC-Box Architecture and Interfaces
- TR-03132 - Sichere Szenarien für Kommunikationsprozesse im Bereich
  hoheitlicher Dokumente
- TR-03133 - Prüfspezifikation zur Technischen Richtlinie BSI TR-03132
  Sichere Szenarien für Kommunikationsprozesse im Bereich hoheitlicher
  Dokumente
- TR-03137 - Optically Verifiable Cryptographic Protection of
  non-electronic Documents
- TR-03139 - Common Certificate Policy for the Extended Access Control
  Infrastructure for Passports and Travel Documents issued by EU Member
- TR-03145 - Secure Certification Authority operation
- TR-03146 - Elektronische Bildübermittlung zur Beantragung hoheitlicher
  Dokumente (E-Bild hD)
- TR-03147 - Vertrauensniveaubewertung von Verfahren zur
  Identitätsprüfung natürlicher Personen
- TR-03150 - Plan for Testing of Contactless Media for Conformance with
  CEN/TS 16794:2017

Also, not in scope for the diagrams are technical specifications of base
technologies and protocols e.g., IETF-RFCs.

### European

The european technical specifications concerning the eIDAS implementation
are represented exhaustively.


## Regulations

All regulatory documents, german and eurpean, have been chosen because
they are referenced in at laest one of the specifications.