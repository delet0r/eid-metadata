# eID-Metadata

DISCLAIMER: This is a work in progress.

This project aims to provide information about the structure of
technical specifications and regulations concerning the german eID as
well as the european eIDAS.

## Structure

Every part of this project contains its own README if necessary
containing more specific information about said part.

### Diagrams

Until now all diagrams are supplied as source files next to the generated svg files
for easy access.

### Texts

This part contains additions to or manipulations of existing texts to provide
better readability e.g., the incorporation of regulatory changes into the original
text such that diff tools can be applied.